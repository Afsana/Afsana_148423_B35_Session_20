<?php
namespace App;

class Person
{
    public $name="Rima";
    public $gender="Female";
    public $blood_group="AB+";

    public function showPersonInfo()
    {
        echo $this->name."<br>";
        echo $this->gender."<br>";
        echo $this->blood_group."<br>";
    }
}
?>